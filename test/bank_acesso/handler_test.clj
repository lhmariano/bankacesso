(ns bank-acesso.handler-test
   (:use clojure.test
         ring.mock.request
         cheshire.core
         bank-acesso.route
         bank-acesso.core))

(deftest test-app
   (testing "not-found route"
      (let [response (app (request :get "/incorrect-route"))]
         (is (= (:status response) 404))
      )
   )

   (testing "Get info account"
      (let [response (app (request :get "/api/bankacesso/info/account/109"))]
         (is (= (:status response) 200))
         (let [body (parse-string (:body response))]
            (is (= (get body "nome") "Huguinho"))
            (is (= (get body "endereco") "Rua do Huguinho, 1"))
            (is (= (get body "telefone") "12121212"))
            (is (= (get body "cpf") "42527379907"))
         )
      )
   )

   (testing "Do deposit"
      (let [response (app (content-type (body (request :post "/api/bankacesso/deposit") (generate-string {:accountnumber 109 :value 100.0})) "application/json"))]
         (println response)
         (is (= (:status response) 201))
         (let [body (parse-string (:body response))]
            (is (= (get body "message") "Deposito realizado com sucesso."))
         )
      )
   )
)
