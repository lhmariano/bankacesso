-- name: register-client!
-- Cadastra um cliente no banco
insert into bancoacesso.tb_cliente (nome, endereco, telefone, cpf)
values (:name, :address, :phone, :cpf)

-- name: create-account!
-- Cria uma conta no banco
insert into bancoacesso.tb_conta (cpfcliente)
values (:cpf)

-- name: get-info-by-account
-- Obtem as informacoes de uma conta e de seu proprietario
select cl.nome, cl.endereco, cl.telefone, cl.cpf, ct.numero
from bancoacesso.tb_cliente as cl, bancoacesso.tb_conta as ct
where cl.cpf = ct.cpfcliente and ct.numero = :accountnumber

-- name: get-info-by-cpf
-- Obtem as informacoes de uma conta e de seu proprietario
select cl.nome, cl.endereco, cl.telefone, cl.cpf, ct.numero
from bancoacesso.tb_cliente as cl, bancoacesso.tb_conta as ct
where cl.cpf = ct.cpfcliente and ct.cpfCliente = :cpf

-- name: drive-account!
-- Realiza um movimento em uma conta
insert into bancoacesso.tb_movimentacao (contaorigem, contadestino, valor)
values (:originaccount, :destinyaccount, :value)

-- name: get-balance
-- Obtem o saldo de uma conta
select sum(m.valor) as saldo
from bancoacesso.tb_movimentacao as m
where ((m.contaorigem = m.contadestino) and m.contaorigem = :accountnumber) or
      ((m.contaorigem <> m.contadestino) and (m.contaorigem = :accountnumber and m.valor < 0)) or
      ((m.contaorigem <> m.contadestino) and (m.contadestino = :accountnumber and m.valor > 0))

-- name: get-extract
-- Obtem o extrato de uma conta
select *
from bancoacesso.tb_movimentacao as m
where ((m.contaorigem = m.contadestino) and m.contaorigem = :accountnumber) or
      ((m.contaorigem <> m.contadestino) and (m.contaorigem = :accountnumber and m.valor < 0)) or
      ((m.contaorigem <> m.contadestino) and (m.contadestino = :accountnumber and m.valor > 0))
