function DataFormatada () {
  this.imprimirData = function () {
    var data = new Date();
    var dia = data.getDate();
    var mes = data.getMonth() + 1;
    var descricaoMes = "";

    switch (mes) {
      case 1: descricaoMes = "Janeiro";
              break;
      case 2: descricaoMes = "Fevereiro";
              break;
      case 3: descricaoMes = "Marco";
              break;
      case 4: descricaoMes = "Abril";
              break;
      case 5: descricaoMes = "Maio";
              break;
      case 6: descricaoMes = "Junho";
              break;
      case 7: descricaoMes = "Julho";
              break;
      case 8: descricaoMes = "Agosto";
              break;
      case 9: descricaoMes = "Setembro";
              break;
      case 10: descricaoMes = "Outubro";
              break;
      case 11: descricaoMes = "Novembro";
              break;
      case 12: descricaoMes = "Dezembro";
              break;
    }

    var ano = data.getFullYear();
    return (dia + " de " + descricaoMes + " de " + ano);
  }
}

function formatDate (date) {
   var dia = date.getDate();
   var mes = date.getMonth() + 1;
   var ano = date.getFullYear();
   return (dia + "/" + mes + "/" + ano);
}

function validateInputAddress (id) {
   var idInput = "#" + id;
   var value = $(idInput).val().replace(/[^A-Za-z0-9.,\343\303\341\301\340\300\342\302\351\311\350\310\352\312\355\315\357\317\363\323\364\324\365\325\366\326\372\332\374\334\347\307\361\321 ]+/g, '');
   $(idInput).val(value);
}

function validateInputName (id) {
   var idInput = "#" + id;
   var value = $(idInput).val().replace(/[^A-Za-z\343\303\341\301\340\300\342\302\351\311\350\310\352\312\355\315\357\317\363\323\364\324\365\325\366\326\372\332\374\334\347\307\361\321 ]+/g, '');
   $(idInput).val(value);
}

function validateInputAccount (id) {
   var idInput = "#" + id;
   var value = $(idInput).val().replace(/[^0-9]+/g, '');
   $(idInput).val(value);
}

function validateInputPhone (id) {
   var idInput = "#" + id;
   var value = $(idInput).val().replace(/[^0-9]+/g, '');
   if (value.length > 9) {
      value = value.substring(0,9);
   }
   $(idInput).val(value);
}

function validateInputCPF (id) {
   var idInput = "#" + id;
   var value = $(idInput).val().replace(/[^0-9]+/g, '');
   if (value.length > 11) {
      value = value.substring(0,11);
   }
   $(idInput).val(value);
}

function validateCPF (cpf) {
   cpf = cpf.replace(/[^\d]+/g,'');

   if(cpf == '')
      return false;

   if (cpf.length != 11 || cpf == "00000000000" || cpf == "11111111111" ||
      cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" ||
      cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" ||
      cpf == "88888888888" || cpf == "99999999999")
      return false;

   add = 0;

   for (i=0; i < 9; i ++)
      add += parseInt(cpf.charAt(i)) * (10 - i);

   rev = 11 - (add % 11);

   if (rev == 10 || rev == 11)
      rev = 0;

   if (rev != parseInt(cpf.charAt(9)))
      return false;

   add = 0;

   for (i = 0; i < 10; i ++)
      add += parseInt(cpf.charAt(i)) * (11 - i);

   rev = 11 - (add % 11);

   if (rev == 10 || rev == 11)
      rev = 0;

   if (rev != parseInt(cpf.charAt(10)))
      return false;

   return true;
}

function validateInputValue (id, precision) {
   var idInput = "#" + id;
   var value = $(idInput).val().replace(/[^0-9.,]+/g,"");

   if ((value.indexOf("..") >= 0) || (value.indexOf(",,") >= 0)) {
      $(idInput).val(value.substring(0, value.length - 1));
      return;
   }

	if ((value.charAt(0) != ".") && (value.charAt(0) != ",")) {
		if (!((value.indexOf(".") >= 0) && (value.indexOf(",") >= 0))) {
			var position = value.indexOf(".");

			if (position == -1) {
            	position = value.indexOf(",");
			}

			if (position >= 0) {
				if ((value.length - position - 1) <= precision) {
					var parts = value.split(".");

					if (parts.length <= 2) {
						parts = value.split(",");

						if (parts.length > 2) {
                     $(idInput).val(value.substring(0, value.length - 1));
                     return;
						}
					} else {
                  $(idInput).val(value.substring(0, value.length - 1));
                  return;
					}
				} else {
               $(idInput).val(value.substring(0, value.length - 1));
               return;
				}
			}

         $(idInput).val(value);
         return;
		}
	}

   $(idInput).val(value.substring(0, value.length - 1));
}

function clearInput () {
   for (var i = 0; i < arguments.length; i++) {
      $("#" + arguments[i]).val("");
   }
}

function show () {
   for (var i = 0; i < arguments.length; i++) {
      $("#" + arguments[i]).show();
   }
}

function hide () {
   for (var i = 0; i < arguments.length; i++) {
      $("#" + arguments[i]).hide();
   }
}

function openAccount (name, address, phone, cpf) {
   var dataClient = { "name": name, "address": address, "phone": phone, "cpf": cpf };
   var result = { "success": false, "message": "Problema ao abrir a conta." };

   $.ajax({
      type: "POST",
      url: "http://localhost:3000/api/bankacesso/account/open",
      dataType: "json",
      contentType: "application/json; charset:UTF-8",
      tradicional: true,
      async: false,
      data: JSON.stringify (dataClient),
      success: function(data) {
         result.success = true;
         result.message = "Cliente cadastrado com sucesso";
      },
      error: function(xhr, status, err) {
         if (xhr.status == 409 || xhr.status == 400) {
            result.success = false;
            result.message = xhr.responseJSON.message;
         } else {
            result.success = false;
            result.message = JSON.stringify(xhr);
         }
      }
   });

   return (result);
}

function alterContent (id, content) {
   $("#" + id).html(content);
}

function getInfoAccount (cpf) {
   var result = { "success": false, "message": "Problema ao obter as informacoes da conta", "info": {} };

   $.ajax({
      type: "GET",
      url: "http://localhost:3000/api/bankacesso/info/account/cpf/" + cpf,
      dataType: "json",
      tradicional: true,
      async: false,
      success: function(data) {
         result.success = true;
         result.message = "Informacoes da conta do cliente obtidas com sucesso";
         result.info = data;
      },
      error: function(xhr, status, err) {
         if (xhr.status == 400) {
            result.success = false;
            result.message = xhr.responseJSON.message;
         } else {
            result.success = false;
            result.message = JSON.stringify(xhr);
         }
      }
   });

   return (result);
}

function filledForm () {
   var filled = true;

   for (var i = 0; i < arguments.length; i++) {
      if ($("#" + arguments[i]).val().trim() == "") {
         filled = false;
      }
   }

   return (filled);
}

function accessAccountClient (account) {
   var result = { "success": false, "message": "Problema ao obter dados da conta.", "info": {} };

   $.ajax({
      type: "GET",
      url: "http://localhost:3000/api/bankacesso/info/account/" + account,
      dataType: "json",
      async: false,
      tradicional: true,
      success: function(data) {
         result.success = true;
         result.message = "Informacoes da conta do cliente obtidas com sucesso";
         result.info = data;
      },
      error: function(xhr, status, err) {
         if (xhr.status == 404) {
            result.success = false;
            result.message = xhr.responseJSON.message;
         } else {
            result.success = false;
            result.message = JSON.stringify(xhr);
         }
      }
   });

   return (result);
}

function doDepositAccount (value, account) {
   var result = { "success": false, "message": "Problema ao fazer deposito na conta.", "info": {} };
   var dataDeposit = { "accountnumber": account, "value": value };

   $.ajax({
      type: "POST",
      url: "http://localhost:3000/api/bankacesso/deposit",
      dataType: "json",
      contentType: "application/json; charset:UTF-8",
      tradicional: true,
      async: false,
      data: JSON.stringify (dataDeposit),
      tradicional: true,
      success: function(data) {
         result.success = true;
         result.message = "Valor de R$" + value.replace(".",",") + " depositado com sucesso.";
      },
      error: function(xhr, status, err) {
         result.success = false;
         result.message = JSON.stringify(xhr);
      }
   });

   return(result);
}

function dotransfer (originaccount, destinyaccount, value) {
   var result = { "success": false, "message": "Problema ao fazer transfer&ecirc;ncia.", "info": {} };
   var dataTransfer = { "originaccount": originaccount, "destinyaccount": destinyaccount, "value": value };

   $.ajax({
      type: "POST",
      url: "http://localhost:3000/api/bankacesso/transfer",
      dataType: "json",
      contentType: "application/json; charset:UTF-8",
      tradicional: true,
      async: false,
      data: JSON.stringify (dataTransfer),
      tradicional: true,
      success: function(data) {
         result.success = true;
         result.message = "Valor de R$" + value.replace(".",",") + " transferido com sucesso.";
      },
      error: function(xhr, status, err) {
         result.success = false;
         result.message = JSON.stringify(xhr);
      }
   });

   return(result);
}

function doWithdrawalAccount (value, account) {
   var result = { "success": false, "message": "Problema ao fazer saque na conta.", "info": {} };
   var dataWithdrawal = { "accountnumber": account, "value": value };

   $.ajax({
      type: "POST",
      url: "http://localhost:3000/api/bankacesso/withdrawal",
      dataType: "json",
      contentType: "application/json; charset:UTF-8",
      tradicional: true,
      async: false,
      data: JSON.stringify (dataWithdrawal),
      tradicional: true,
      success: function(data) {
         result.success = true;
         result.message = "Valor de R$" + value.replace(".",",") + " sacado com sucesso.";
      },
      error: function(xhr, status, err) {
         result.success = false;
         result.message = JSON.stringify(xhr);
      }
   });

   return(result);
}

function showBalance (account) {
   var result = { "success": false, "message": "Problema ao ober o saldo.", "info": {} };

   $.ajax({
      type: "GET",
      url: "/api/bankacesso/account/balance/" + account,
      dataType: "json",
      tradicional: true,
      async: false,
      success: function(data) {
         result.success = true;
         result.message = "Saldo obtido com sucesso."
         result.info = data;
      },
      error: function(xhr, status, err) {
         result.success = false;
         result.message = xhr.responseJSON.message;
      }
   });

   return(result);
}

function showExtract (account) {
   var result = { "success": false, "message": "Problema ao obter o extrato.", "info": {} };

   $.ajax({
      type: "GET",
      url: "/api/bankacesso/account/extract/" + account,
      dataType: "json",
      tradicional: true,
      async: false,
      success: function(data) {
         result.success = true;
         result.message = "Extrato obtido com sucesso."
         result.info = data;
      },
      error: function(xhr, status, err) {
         result.success = false;
         result.message = xhr.responseJSON.message;

      }
   });

   return(result);
}

function createLabelExtract () {
   var table = $("<table>");
   table.addClass("lineLabelExtract");

   var lineLabels = $("<tr>");

   var columnLabelDate = $("<td>");
   columnLabelDate.addClass("dataExtract");
   columnLabelDate.html("Data");

   var columnLabelOperation = $("<td>");
   columnLabelOperation.addClass("dataExtract");
   columnLabelOperation.html("Opera&ccedil;&atilde;o");

   var columnLabelValue = $("<td>");
   columnLabelValue.addClass("dataExtract");
   columnLabelValue.html("Valor(R$)");

   lineLabels.append(columnLabelDate);
   lineLabels.append(columnLabelOperation);
   lineLabels.append(columnLabelValue);

   table.append(lineLabels);

   $("#extract").append(table);
}

function createBalanceExtract (balance) {
   var table = $("<table>");
   table.addClass("lineBalanceExtract");
   var line = $("<tr>");
   var columnBalance = $("<td>");
   columnBalance.addClass("dataExtract");
   columnBalance.html("Saldo:");
   var columnBlank = $("<td>");
   columnBlank.addClass("dataExtract");
   columnBlank.html("");
   var columnValue = $("<td>");
   columnValue.addClass("dataExtract");
   columnValue.html(balance.toFixed(2).replace(".",","));
   line.append(columnBalance);
   line.append(columnBlank);
   line.append(columnValue);
   table.append(line);
   $("#extract").append(table);
}

function createExtract (movements) {
   createLabelExtract();

   var balance = 0;

   for (var i = 0; i < movements.length; i++) {
      var table = $("<table>");
      table.addClass("lineExtract");

      var line = $("<tr>");

      var columnDate = $("<td>");
      columnDate.addClass("dataExtract");
      columnDate.html(formatDate(new Date(movements[i].data)));

      var columnOperation = $("<td>");
      columnOperation.addClass("dataExtract");

      if ((movements[i].contaorigem == movements[i].contadestino) && (movements[i].valor > 0)) {
         columnOperation.html("Dep&oacute;sito");
      } else {
         if ((movements[i].contaorigem == movements[i].contadestino) && (movements[i].valor < 0)) {
            columnOperation.html("Saque");
         } else {
            var originDestiny = "";

            if (movements[i].valor > 0) {
               originDestiny = "Transf&ecirc;rencia<br>Origem: ";
               var client = accessAccountClient(movements[i].contaorigem)
               originDestiny += client.info.nome;
               columnOperation.html(originDestiny);
            } else {
               originDestiny = "Transf&ecirc;rencia<br>Destino: ";
               var client = accessAccountClient(movements[i].contadestino)
               originDestiny += client.info.nome;
               columnOperation.html(originDestiny);
            }
         }
      }

      var columnValue = $("<td>");
      columnValue.addClass("dataExtract");
      columnValue.html(movements[i].valor.toFixed(2).replace(".",","));
      balance += movements[i].valor;

      line.append(columnDate);
      line.append(columnOperation);
      line.append(columnValue);

      table.append(line);

      $("#extract").append(table);
   }

   createBalanceExtract(balance);
}

/*
       dec octal
   ã = 227 343
   Ã = 195 303
   á = 225 341
   Á = 193 301
   à = 224 340
   À = 192 300
   â = 226 342
   Â = 194 302
   é = 233 351
   É = 201 311
   è = 232 350
   È = 200 310
   ê = 234 352
   Ê = 202 312
   í = 237 355
   Í = 205 315
   ï = 239 357
   Ï = 207 317
   ó = 243 363
   Ó = 211 323
   ô = 244 364
   Ô = 212 324
   õ = 245 365
   Õ = 213 325
   ö = 246 366
   Ö = 214 326
   ú = 250 372
   Ú = 218 332
   ü = 252 374
   Ü = 220 334
   ç = 231 347
   Ç = 199 307
   ñ = 241 361
   Ñ = 209 321
*/
