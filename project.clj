(defproject bank-acesso "0.1.0-SNAPSHOT"
  :description "Implementação de sistema web em Clojure para transações bancárias (home banking)."
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [ring/ring-core "1.2.1"]
                 [ring/ring-defaults "0.2.1"]
                 [ring/ring-json "0.4.0"]
                 [ring-json-response "0.2.0"]
                 [compojure "1.5.1"]
                 [cheshire "5.6.3"]
                 [org.clojure/tools.logging "0.3.1"]
                 [log4j/log4j "1.2.17" :exclusions [javax.mail/mail javax.jms/jms com.sun.jdmk/jmxtools com.sun.jmx/jmxri]]
                 [environ "1.1.0"]
                 [yesql "0.5.3"]
                 [org.clojure/java.jdbc "0.6.2-alpha3"]
                 [org.postgresql/postgresql "9.4-1201-jdbc41"]]
  :plugins [[lein-environ "1.1.0"]
            [lein-codox "0.10.1"]
            [lein-marginalia "0.9.0"]
            [lein-ring "0.9.7"]] ; Permite iniciar um web server com 'lein ring server', empacotar a aplicação como um standalone .jar ou .war para fazer deploy em conteiner servlet.
  :ring {:handler bank-acesso.core/app}
  :profiles {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                                  [ring-mock "0.1.5"]]
                   :jvm-opts ["-Dlogfile.path=development"]}
             :test {:jvm-opts ["-Dlogfile.path=test"]}
             :production {:jvm-opts ["-Dlogfile.path=production"]}}
)
