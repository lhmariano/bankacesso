(ns bank-acesso.model
   (:require [environ.core :refer [env]]
             [clojure.java.jdbc :as jdbc]
             [yesql.core :refer [defquery defqueries]]))

(def db {
  :subprotocol (env :subprotocol)
  :subname (env :subname)
  :user (env :user)
  :password (env :password)
})

(defqueries "sql/consultas.sql" {:connection db})

(defn open-account [name address phone cpf]
   (jdbc/with-db-transaction [ts db]
      (register-client! {:name name :address address :phone phone :cpf cpf} {:connection ts})
      (create-account! {:cpf cpf} {:connection ts})
   )
)
