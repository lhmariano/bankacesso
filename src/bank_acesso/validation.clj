(ns bank-acesso.validation
   (:require [bank-acesso.util :refer :all]))

(defn validate-data-get-info-account [accountnumber]
   (verify-positive-integer accountnumber)
)

(defn validate-data-get-info-account-cpf [cpf]
   (verifiy-cpf cpf)
)

(defn validate-data-get-balance-account [accountnumber]
   (verify-positive-integer accountnumber)
)

(defn validate-data-get-extract-account [accountnumber]
   (verify-positive-integer accountnumber)
)

(defn validate-data-register-client-bank [name address phone cpf]
   (if (and (verify-name name) (verify-address address) (verify-phone-number phone) (verifiy-cpf cpf)) true false)
)

(defn validate-data-open-account-bank [name address phone cpf]
   (validate-data-register-client-bank name address phone cpf)
)

(defn validate-data-create-account-bank [cpf]
   (verifiy-cpf cpf)
)

(defn validate-data-do-deposit [accountnumber value]
   (if (and (verify-positive-integer accountnumber) (verify-positive-real value)) true false)
)

(defn validate-data-do-withdrawal [accountnumber value]
   (validate-data-do-deposit accountnumber value)
)

(defn validate-data-do-transfer [originaccount destinyaccount value]
   (if (and (verify-positive-integer originaccount) (verify-positive-integer destinyaccount) (verify-positive-real value)) true false)
)
