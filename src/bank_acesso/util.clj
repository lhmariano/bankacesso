(ns bank-acesso.util)

; Recebe um string que representa um numero inteiro e retorna o numero inteiro correspondente.
(defn string-to-integer [string]
   (Integer. string)
)

; Separa os caracteres de um string e os retorna em uma lista.
(defn split-string [string]
   (for [i (range 0 (.length string))]
      (subs string i (inc i))
   )
)

(defn list-string-to-integer [list]
   (for [i (range 0 (count list))]
      (string-to-integer (nth list i))
   )
)

; Recebe duas listas de mesmo tamanho e retorna uma lista onde o n-esimo elemento e a multiplicacao
; do n-esimo elemento da primeira lista com o n-esimo elemento da segunda lista.
(defn multiply-list-numbers [length-list first-list second-list]
   (for [i (range 0 length-list)]
      (* (nth first-list i) (nth second-list i))
   )
)

(defn sum-elements-list [list]
   (reduce + list)
)

(defn calculate-first-digit-verifier [digits-cpf]
   (let [digit (mod (* (sum-elements-list (multiply-list-numbers 9 digits-cpf (range 10 1 -1))) 10) 11)]
      (if (> digit 9) 0 digit)
   )
)

(defn calculate-second-digit-verifier [digits-cpf]
   (let [digit (mod (* (sum-elements-list (multiply-list-numbers 10 digits-cpf (range 11 1 -1))) 10) 11)]
      (if (> digit 9) 0 digit)
   )
)

(defn verify-if-letter [caracter]
   (if (or (and (<= (compare "A" caracter) 0) (>= (compare "Z" caracter) 0))
           (and (<= (compare "a" caracter) 0) (>= (compare "z" caracter) 0)))
      true
      false
   )
)

(defn verify-if-digit [caracter]
   (if (and (<= (compare "0" caracter) 0) (>= (compare "9" caracter) 0)) true false)
)

(defn verify-only-digit [string]
   (if (= (re-matches #"[0-9]+" string) string) true false)
)

(defn verify-only-letter [string]
   (if (= (re-matches #"[A-Za-z]+" string) string) true false)
)

(defn verify-name [string]
   (if (= (re-matches #"[A-Za-z ãÃáÁàÀÂâéÉÈèêÊíÍïÏóÓôÔõÕöÖúÚüÜçÇñÑ]+" string) string) true false)
)

(defn verify-address [string]
   (if (= (re-matches #"[A-Za-z 0-9,.ãÃáÁàÀÂâéÉÈèêÊíÍïÏóÓôÔõÕöÖúÚüÜçÇñÑ]+" string) string) true false)
)

(defn verify-phone-number [string]
   (if (and (verify-only-digit string) (or (= (.length string) 8) (= (.length string) 9))) true false)
)

(defn verify-invalid-cpf [cpf]
   (if (or (= cpf "00000000000")
           (= cpf "11111111111")
           (= cpf "22222222222")
           (= cpf "33333333333")
           (= cpf "44444444444")
           (= cpf "55555555555")
           (= cpf "66666666666")
           (= cpf "77777777777")
           (= cpf "88888888888")
           (= cpf "99999999999"))
      true
      false
   )
)

(defn verifiy-cpf [cpf]
   (if (and (= (.length cpf) 11) (verify-only-digit cpf) (not (verify-invalid-cpf cpf)))
      (let [digits-cpf (list-string-to-integer (split-string cpf))]
         (if (= (calculate-first-digit-verifier digits-cpf) (nth digits-cpf 9))
            (if (= (calculate-second-digit-verifier digits-cpf) (nth digits-cpf 10))
               true
               false
            )
            false
         )
      )
      false
   )
)

(defn verify-positive-integer [value]
   (try
      (let [number (Integer. value)] (if (> number 0) true false))
      (catch Exception e false)
   )
)

(defn verify-positive-real [value]
   (try
      (let [number (Float. value)] (if (> number 0) true false))
      (catch Exception e false)
   )
)
