(ns bank-acesso.route
   (:require [compojure.core :refer :all]
             [compojure.route :as route]
             [clojure.tools.logging :as log]
             [bank-acesso.handler :refer :all]))

(defroutes app-routes
   (GET "/api/bankacesso/info/account/:accountnumber" [accountnumber]
      (log/info "> Obter informacoes da conta pelo numero da conta.")
      (get-info-account accountnumber))

   (GET "/api/bankacesso/info/account/cpf/:cpf" [cpf]
      (log/info "> Obter informacoes da conta pelo CPF.")
      (get-info-account-cpf cpf))

   (GET "/api/bankacesso/account/balance/:accountnumber" [accountnumber]
      (log/info "> Obter saldo da conta.")
      (get-balance-account accountnumber))

   (GET "/api/bankacesso/account/extract/:accountnumber" [accountnumber]
      (log/info "> Obter extrato da conta.")
      (get-extract-account accountnumber))

   (POST "/api/bankacesso/account/create/:cpf" [cpf]
      (log/info "> Criar uma conta e associar um CPF.")
      (create-account-bank cpf))

   (POST "/api/bankacesso/deposit" req
      (let [accountnumber (get-in req [:body :accountnumber])
            value (get-in req [:body :value])]
         (log/info "> Realizar um deposito na conta.")
         (do-deposit accountnumber value)))

   (POST "/api/bankacesso/withdrawal" req
      (let [accountnumber (get-in req [:body :accountnumber])
            value (get-in req [:body :value])]
         (log/info "> Realizar um saque na conta.")
         (do-withdrawal accountnumber value)))

   (POST "/api/bankacesso/transfer" req
      (let [originaccount (get-in req [:body :originaccount])
            destinyaccount (get-in req [:body :destinyaccount])
            value (get-in req [:body :value])]
         (log/info "> Realizar uma transferencia da conta.")
         (do-transfer originaccount destinyaccount value)))

   (POST "/api/bankacesso/client/register" req
      (let [name (get-in req [:body :name])
            address (get-in req [:body :address])
            phone (get-in req [:body :phone])
            cpf (get-in req [:body :cpf])]
         (log/info "> Cadastrar um cliente no banco.")
         (register-client-bank name address phone cpf)))

   (POST "/api/bankacesso/account/open" req
      (let [name (get-in req [:body :name])
            address (get-in req [:body :address])
            phone (get-in req [:body :phone])
            cpf (get-in req [:body :cpf])]
         (log/info "> Realizar uma abertura de conta.")
         (open-account-bank name address phone cpf)))

   (route/resources "/") ; Rota para arquivos estáticos

   (route/not-found "Not Found"))
