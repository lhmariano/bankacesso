(ns bank-acesso.core
   (:require [ring.middleware.json :refer [wrap-json-body wrap-json-response]]
             [bank-acesso.route :refer :all]))

(def app (wrap-json-response (wrap-json-body app-routes {:keywords? true})))
; wrap-json-body - faz o parse do body de qualquer requisicao com Content-Type JSON em uma estrutura de dados Clojure e atribui para a chave :body
; wrap-json-response - converte o corpo de uma resposta em um JSON, desde que o corpo seja uma colecao Clojure (map, vector, set, etc)
