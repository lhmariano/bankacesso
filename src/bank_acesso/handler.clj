(ns bank-acesso.handler
   (:require [bank-acesso.validation :refer :all]
             [bank-acesso.model :refer :all]
             [ring.util.response :refer :all]))

(defn response-200 [information]
   (status (response information) 200)
)

(defn response-201 [message]
   (status (response {:message message}) 201)
)

(defn response-400 [message]
   (status (response {:message message}) 400)
)

(defn response-404 [message]
   (status (response {:message message}) 404)
)

(defn response-409 [message]
   (status (response {:message message}) 409)
)

(defn get-info-account [accountnumber]
   (if (validate-data-get-info-account accountnumber)
      (let [info (first (get-info-by-account {:accountnumber (Integer. accountnumber)}))]
         (if (not (= info nil))
            (response-200 info)
            (response-404 "Nenhuma informacao encontrada para a conta fornecida.")
         )
      )
      (response-400 "Formato do numero da conta incorreto.")
   )
)

(defn get-info-account-cpf [cpf]
   (if (validate-data-get-info-account-cpf cpf)
      (let [info (first (get-info-by-cpf {:cpf cpf}))]
         (if (not (= info nil))
            (response-200 info)
            (response-404 "Nenhuma informacao encontrada para o cpf fornecida.")
         )
      )
      (response-400 "CPF invalido.")
   )
)

(defn get-balance-account [accountnumber]
   (if (validate-data-get-balance-account accountnumber)
      (let [info (first (get-balance {:accountnumber (Integer. accountnumber)}))]
         (if (= (get-in info [:saldo]) nil)
            (let [informacao {:saldo 0}] (response-200 informacao))
            (response-200 info)
         )
      )
      (response-400 "Formato do numero da conta incorreto.")
   )
)

(defn get-extract-account [accountnumber]
   (if (validate-data-get-extract-account accountnumber)
      (let [info (get-extract {:accountnumber (Integer. accountnumber)})]
         (if (= info [])
            (response-400 "A conta informada nao possui movimentacao.")
            (response-200 info)
         )
      )
      (response-400 "Formato do numero da conta incorreto.")
   )
)

(defn do-deposit [accountnumber value]
   (println accountnumber value)
   (if (validate-data-do-deposit accountnumber value)
      (do
         (try
            (drive-account! {:originaccount accountnumber :destinyaccount accountnumber :value (Float. value)})
            (response-201 "Deposito realizado com sucesso.")
            (catch Exception e (response-400 (str "Nao foi possivel realizar o deposito. Erro: " (.toString e))))
         )
      )
      (response-400 "Dados do deposito incorretos.")
   )
)

(defn do-withdrawal [accountnumber value]
   (if (validate-data-do-withdrawal accountnumber value)
      (do
         (try
            (drive-account! {:originaccount accountnumber :destinyaccount accountnumber :value (* -1 (Float. value))})
            (response-201 "Saque realizado com sucesso.")
            (catch Exception e (response-400 (str "Nao foi possivel realizar o saque. Erro: " (.toString e))))
         )
      )
      (response-400 "Dados do saque incorretos.")
   )
)

(defn do-transfer [originaccount destinyaccount value]
   (if (validate-data-do-transfer originaccount destinyaccount value)
      (do
         (try
            (drive-account! {:originaccount originaccount :destinyaccount destinyaccount :value (* -1 (Float. value))})
            (drive-account! {:originaccount originaccount :destinyaccount destinyaccount :value (Float. value)})
            (response-201 "Transferencia realizado com sucesso.")
            (catch Exception e (response-400 (str "Nao foi possivel realizar a transferenica. Erro: " (.toString e))))
         )
      )
      (response-400 "Dados da transferencia incorretos.")
   )
)

(defn create-account-bank [cpf]
   (if (validate-data-create-account-bank cpf)
      (do
         (try
            (create-account! {:cpf cpf})
            (response-201 "Conta criada com sucesso.")
            (catch java.sql.BatchUpdateException e
               (let [exception (str (.getNextException e))]
                  (if (.contains exception "violates foreign key constraint")
                     (response-400 "Nao existe nenhum cliente cadastrado com o CPF fornecido.")
                     (response-400 "Ja existe uma conta criada para o CPF fornecido.")
                  )
               )
            )
            (catch Exception e (response-400 (str "Nao foi possivel criar a conta para o cliente. Erro: " (.toString e))))
         )
      )
      (response-400 "O CPF fornecido esta incorreto")
   )
)

(defn register-client-bank [name address phone cpf]
   (if (validate-data-register-client-bank name address phone cpf)
      (do
         (try
            (register-client! {:name name :address address :phone phone :cpf cpf})
            (response-201 "Cadastro de cliente realizado com sucesso.")
            (catch java.sql.BatchUpdateException e (response-409 "CPF já cadastrado."))
            (catch Exception e (response-400 (str "Nao foi possivel realizar o cadastro do cliente. Erro: " (.toString e))))
         )
      )
      (response-400 "Dados do cliente incorretos.")
   )
)

(defn open-account-bank [name address phone cpf]
   (if (validate-data-open-account-bank name address phone cpf)
      (do
         (try
            (open-account name address phone cpf)
            (response-201 "Conta aberta com sucesso.")
            (catch java.sql.BatchUpdateException e (response-409 "CPF já cadastrado."))
            (catch Exception e (response-400 (str "Nao foi possivel realizar o cadastro do cliente. Erro: " (.toString e))))
         )
      )
      (response-400 "Dados do cliente incorretos.")
   )
)
