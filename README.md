# bank-acesso

Implementação de sistema web em Clojure para transações bancárias (home banking).

# Pré-requisitos

Será necessário [Leiningen][] 2.0.0 ou superior.

[leiningen]: https://github.com/technomancy/leiningen

# Execução

Para iniciar o servidor web para a aplicação execute o seguinte comando: lein ring server


